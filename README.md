# Loker Logger

## How to run

```
virtualenv env
source env/bin/activate
pip install -r requirements.txt
FLASK_APP=app.py thon -m flask run -p 4000
```

## How to use

Send POST request to http://localhost:4000/log with parameter:
- uid : (UID cardnumber)
- status : (OPEN / CLOSE)
from flask import Flask, jsonify, request, render_template
from datetime import datetime
import json

app = Flask(__name__)

with open("log.json", "r") as logFile:
    logger = json.load(logFile)
    logFile.close()

@app.route("/")
def home():
    return "Go to /log"

# Send POST request with uid and status (CLOSE/OPEN)
# to add log to file
@app.route('/log', methods = ['GET', 'POST'])
def add_log():
    if request.method == 'GET':
        return render_template('log.html', content=logger)

    elif request.method == 'POST':
        now = datetime.now()
        ts = now.strftime("%d/%m/%Y %H:%M:%S")
        print(ts)
        uid = request.form.get('uid')
        status = str.upper(request.form.get('status'))
        log = {'timestamp':ts, 'uid':uid, 'status':status}
        logger.append(log)
        with open('log.json', 'w') as logFile:
            json.dump(logger, logFile, indent=2)
    
    return jsonify(logger)

if __name__ == "__main__":
    app.run(debug=True)
    logFile.close()